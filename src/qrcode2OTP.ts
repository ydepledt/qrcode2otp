// Import required modules
import { createCanvas, loadImage } from 'canvas';
import jsQR from 'jsqr';
import fs from 'fs/promises';
import path from 'path';
import { TOTP } from "otpauth";

interface QRCodeData {
  secret: string,
  digits: number,
  algorithm: string,
  issuer: string,
  period: number
}

/**
 * Creates the path for the QR code image file.
 * @param qrCodeFolderPath - The folder path where the QR code image is located.
 * @returns The path to the QR code image file.
 */
async function createPath(qrCodeFolderPath: string): Promise<string> {
  try {
    const files = await fs.readdir(qrCodeFolderPath);

    if (files.length === 0) {
      console.log('No files found in the folder.');
      return '';
    }

    // Get the first file in the folder
    const imageFileName = files[0];
    const qrCodePath = path.join(qrCodeFolderPath, imageFileName);

    return qrCodePath;

  } catch (err) {
    console.error('Error reading folder:', err);
    throw err;
  }
}

/**
 * Reads the QR code from an image file and returns corresponding TOTP.
 * @param qrCodeFolderPath - The folder path where the QR code image is located.
 * @param imagePath - The path to the image file.
 * @returns The OTP (One-Time Password) generated from the QR code, or null if no QR code is found.
 */
export async function readQRCodeFromImage(qrCodeFolderPath: string = 'qrcode', imagePath?: string): Promise<string|null> {
  try {
    // Determine the path to the QR code image file
    const pathToQrcode = imagePath || await createPath(qrCodeFolderPath);

    // Load the image file
    const image = await loadImage(pathToQrcode);

    // Create a canvas with the same dimensions as the image
    const canvas = createCanvas(image.width, image.height);
    const context = canvas.getContext('2d');

    // Draw the image onto the canvas
    context.drawImage(image, 0, 0);

    // Get the raw image data from the canvas
    const imageData = context.getImageData(0, 0, image.width, image.height).data;

    // Use jsQR to decode the QR code from the image data
    const code = jsQR(imageData, image.width, image.height);

    if (code) {
      // Extract QR code data
      const { secret, digits, algorithm, issuer, period } = extractQRCodeData(code.data);

      // Delete the QR code image file
      await fs.unlink(pathToQrcode);

      // Generate the TOTP (One-Time Password)
      return generateTOTP(secret, digits, algorithm, issuer, period);

    } else {
      // No QR code found in the image
      console.log('No QR Code found in the image.');
      return null;
    }
  } catch (err) {
    console.error(err);
    throw err;
  }
}

/**
 * Extracts QR code data from the QR code link.
 * @param qrCodeLink - The QR code link.
 * @returns An object containing the extracted QR code data.
 * @throws Error if the QR code link format is invalid.
 */
function extractQRCodeData(qrCodeLink: string): QRCodeData {
  const regex = /^otpauth:\/\/totp\/[^:]+:[^?]+\?secret=([^&]+)&digits=(\d+)&algorithm=([^&]+)&issuer=([^&]+)&period=(\d+)$/;

  const match = qrCodeLink.match(regex);
  if (!match) {
    throw new Error("Invalid QR code link format");
  }

  const [, secret, digits, algorithm, issuer, period] = match;

  return {
    secret,
    digits: parseInt(digits),
    algorithm,
    issuer,
    period: parseInt(period)
  };
}

/**
 * Generates a TOTP (One-Time Password) using the provided parameters.
 * @param secret - The secret key for the TOTP.
 * @param digits - The number of digits in the generated OTP.
 * @param algorithm - The algorithm used for generating the OTP.
 * @param issuer - The issuer of the TOTP.
 * @param period - The time period (in seconds) for which the OTP is valid.
 * @returns The generated OTP.
 */
function generateTOTP(secret: string, digits: number, algorithm: string, issuer: string, period: number): string {
  // Create a new TOTP object.
  const totp = new TOTP({
    issuer,
    algorithm,
    digits,
    period,
    secret,
  });

  return totp.generate();
}

// Get the folder path and image path from command-line arguments
const folderPath: string = process.argv[2];
const imagePath: string = process.argv[3];

// Read the QR code from the image and log the OTP
readQRCodeFromImage(folderPath, imagePath)
  .then(otp => {
    console.log('\nTOTP : ', otp, );
  })
  .catch(err => {
    console.error('Failed to read QR code from image:', err);
  });