# QR Code to OTP Converter

This is a Node.js application that reads a QR code from an image file and generates a One-Time Password (OTP) based on the QR code data. The generated OTP can be used for various purposes, such as two-factor authentication (2FA).

## Prerequisites

- Node.js
- NPM (Node Package Manager)

## Installation

1. Clone this repository to your local machine or download the source code as a ZIP file.
2. Navigate to the project directory in your terminal.
3. Install the dependencies by running the following command:

   ```bash
   npm install
   ```

## Usage

1.Place the QR code image file in a folder.
2.Open a terminal and navigate to the project directory.
3.Run the following command to start the application:

    ```bash
    npm run readQRCode <folderPath> <imagePath>
    ```

Replace <folderPath> with the path to the folder containing the QR code image, and <imagePath> with the path to the image file itself. For example:

    ```bash
    npm run readQRCode ./qrcode-folder ./qrcode-folder/qrcode.png
    ```

Note: If you don't provide the folder path and image path as command-line arguments, the application will look for the QR code image in the qrcode folder by default.

4. The application will read the QR code from the image file and display the generated OTP (One-Time Password) in the terminal.
